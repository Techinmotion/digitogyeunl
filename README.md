# Why We Like Gadgets #

I ask myself this question many times and I still have a fascination for gadgets as I think many people do and I believe that men even have more of a fascination for gadgets, more than women. It may be the newness or coolness but down inside all of us we have that mindset to like new things that are fascinating to our minds. I would like to place gadgets in several different categories.

Geeky Gadgets-This category would take in most people who have a desire for electronic gadgets starting with computers to simple electronic marvels that come and go with our times. Many geeky gadgets may be a new fascination for a new type mouse for a computer or even an electronic lighter.

Gadgets for Men-In my opinion, men are more intrigued by gadgets than even children. Men like to see something that they can touch or feel, and not necessary an electronic gadget, although I am personally fascinated my anything associated with computers especially wi-fi or Bluetooth gadgets.

Gadgets for Women-Women like gadgets for personal hygiene, hair gadgets that make quick work of fixing their hair, and especially kitchen gadgets to make life easier for them.

Electronic Gadgets-I know this is a broad category, but everyone likes gadgets that make our lives simpler and yet can be more appealing to one group more that another depending on what type gadget it might be. The latest gadgets like the Echo Dot and Home are just a couple of the artificial devices you speak to and they talk back to you once you give them voice commands. Just look at our smart phones which can control our lights, security systems, or order products online. Just think of how advanced the GPS (global positioning satellite) as become, able to find just about any place on the globe. Electronics as become a big part of our lives, just about anything with the name "smart" in front of it can be a new electronic gadget.

Stupid and Funny Gadgets-I group these two together because if you're a practical joker then there is a gadget for you, it may be spy camera inside of a ball point pen, or a camera attached to the lapel of a shirt or jacket. How about a box that does nothing, stupid to some people and very funny to other persons? There are political gadgets and ones that will just give your ideas for your next party.
So why do we like gadgets? Psychologists believe that we want to have what others have, for instance the release of a new smart phone you will find people standing in line just to be in the "in crowd". Marketers use many clever means to sell new gadgets, that way they can entice you to feel that you must have that special item. These studies are based on market research. A new field has opened called "neromarketing" which is the science of smart marketing whereby businesses seek to understand consumer behavior and why they are drawn to certain products such as gadgets.

[https://digitogy.eu/nl/](https://digitogy.eu/nl/)